"""
get Environment Variables
"""

import logging
import os

logging_levels = {
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
    "WARNING": logging.WARNING,
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
}

HEATER1_PIN = int(os.getenv("SAUNA_HEATER1_PIN", "16"), 10)
HEATER2_PIN = int(os.getenv("SAUNA_HEATER2_PIN", "18"), 10)
HEATER3_PIN = int(os.getenv("SAUNA_HEATER3_PIN", "22"), 10)
MAIN_SWITCH_PIN = int(os.getenv("SAUNA_MAIN_SWITCH", "32"), 10)

API_LOOPBACK_TIMEOUT = int(os.getenv("SAUNA_API_LOOPBACK_TIMEOUT", 30))
CONTROLLER_TIMEOUT = int(os.getenv("SAUNA_CONTROLLER_LOOPBACK_TIMEOUT", 10))
CONTROLLER_SIMULATION = bool(
    os.getenv("SAUNA_CONTROLLER_SIMULATION", "False").lower()
    in ("true", "1", "t")
)

LOGGING_LEVEL = logging_levels[os.getenv("SAUNA_LOGGING_LEVEL", "INFO").upper()]
LOG_FILE = os.getenv("SAUNA_LOG_FILE", "logs/sauna.log")

MQTT_SERVER = os.getenv("SAUNA_MQTT_SERVER", "localhost")
MQTT_PORT = int(os.getenv("SAUNA_MQTT_PORT", 1883))
MQTT_API_LOOPBACK_TOPIC = os.getenv(
    "SAUNA_MQTT_API_LOOPBACK_TOPIC", "sauna/api/loopback/default"
)
MQTT_CONTROLLER_LOOPBACK_TOPIC = os.getenv(
    "SAUNA_MQTT_CONTROLLER_LOOPBACK_TOPIC", "sauna/controller/loopback/default"
)
CONTROLLER_WATCHDOG_TIMEOUT = int(os.getenv("SAUNA_PROTOCOL_WATCHDOG", 60))
CONTROLLER_WATCHDOG_TOPIC = os.getenv(
    "SAUNA_PROTOCOL_WATCHDOG_TOPIC", "sauna/protocol"
)
CONTROLLER_VERSION = os.getenv("SAUNA_CONTROLLER_VERSION", "0.0.0")

SAUNA_MAX_TIME = int(os.getenv("SAUNA_MAX_TIME", "3600"), 10)
SAUNA_PID_SAMPLE_TIME = int(os.getenv("SAUNA_PID_SAMPLE_TIME", "300"), 10)

SERIAL_PORT = os.getenv("SAUNA_SERIAL_PORT", "")
SERIAL_BAUDRATE = int(os.getenv("SAUNA_SERIAL_BAUDRATE", 115200))
SERIAL_DEVICE_DRIVER = os.getenv("SAUNA_SERIAL_DEVICE_DRIVER", "cp210x")

PID_KP = float(os.getenv("SAUNA_PID_KP", "2.4"))
PID_KI = float(os.getenv("SAUNA_PID_KI", "0.5"))
PID_KD = float(os.getenv("SAUNA_PID_KD", "0.01"))
