"""
sauna control supervisor
"""
import logging
import time
from dataclasses import asdict
from queue import Empty, Queue
from threading import Thread
from typing import Any, Dict, FrozenSet, List, Tuple

import paho.mqtt.client as mqtt

from ..environment import (
    CONTROLLER_TIMEOUT,
    MQTT_CONTROLLER_LOOPBACK_TOPIC,
    MQTT_PORT,
    MQTT_SERVER,
)
from ..objects import LoopbackTask

LOG = logging.getLogger(__name__)


class Monitor(Thread):
    """
    The Monitor checks for every Loopback Task if a Loopback message was
    received on the MQTT message queue
    """

    def __init__(self, queue: Queue[Tuple[str, str]]) -> None:
        super().__init__()
        self._loopback_tasks: FrozenSet[LoopbackTask] = frozenset()
        self._queue: Queue[Tuple[str, str]] = queue
        self.running = False
        self.stopped = False
        self._client: mqtt.Client

    @property
    def _last_run(self) -> int:
        now = int(time.monotonic())
        return now

    def start_monitor(self):
        LOG.info("Starting Monitor")
        self.stopped = False
        # task = self._loop.create_task(self._run())
        for task in self._loopback_tasks:
            LOG.debug(f"Task: {task.topic} - > Timeout: {task.delay}")
            LOG.debug(f"{task}")
        return self.running

    def _stop(self) -> bool:
        LOG.warning("Monitor stop activated")

        sleep_count: int = 0
        self.stopped = True
        while self.running:
            time.sleep(0.1)
            sleep_count += 1
            if sleep_count > 50:
                LOG.warning("Monitor did not stop in time")
        return self.running

    def run(self):
        """
        API Monitoring loop
        """
        self.running = True
        self.stopped = False
        self._client.loop_start()
        LOG.info("Monitor started")
        for task in self._loopback_tasks:
            LOG.info(f"Task: {task.topic} - > Timeout: {task.delay}")
        LOG.info("Running")
        while not self.stopped:
            time.sleep(0.01)
            try:
                while self._queue.not_empty:
                    topic, data = self._queue.get_nowait()
                    self._client.publish(topic, data)
                    self._queue.task_done()
            except Empty:
                pass

            for task in self._loopback_tasks:
                now = self._last_run
                if now > task.last_run + task.delay:
                    if now < task.last_run + task.delay + 5:
                        task.last_run = now
                        continue
                    if task.fail is True and task.last_run < now + 30:
                        task.last_run = now
                        LOG.critical(f"Loopback for {task.topic} failed")
                    task.fail = True

        self._client.loop_stop(force=True)
        self.running = False
        LOG.critical("Monitor stopped")

    def _on_msg(self, client: mqtt.Client, obj: Any, msg: mqtt.MQTTMessage):
        """
        callback if message received
        """
        data: str = msg.payload.decode("UTF-8")
        topic = msg.topic
        LOG.debug(f"Topic:{topic} -> {data}")
        for task in self._loopback_tasks:
            if task.topic == topic:
                LOG.debug(f"Found Task for Topic {topic}")
                task.last_run = self._last_run
                task.fail = False
                break

    def append_task(self, task: LoopbackTask):
        """
        append new `LoopbackTask` to loopback task list
        """
        LOG.debug(f"Appending task: {task.topic}")
        LOG.debug(f"Task Count: {len(self._loopback_tasks)}")
        temp_task: List[LoopbackTask] = []
        for _ in self._loopback_tasks:
            if _.topic == task.topic:
                LOG.debug(f"Updating {task.topic}")
                temp_task.append(task)
            else:
                LOG.debug(f"Adding {_.topic}")
                temp_task.append(_)
        temp_task.append(task)
        self._loopback_tasks = frozenset(temp_task)
        LOG.debug(f"Task Count: {len(self._loopback_tasks)}")
        return self.list_tasks()

    def list_tasks(self) -> List[Dict[str, Any]]:
        return [asdict(_) for _ in self._loopback_tasks]

    def _disconnect(self, client: mqtt.Client, obj: Any, reason_code: int):
        pass

    def setup(self):
        """
        initialize the MQTT client with hostname and port. Register on_message
        callback. Subscribe to the sauna loopback topic. Register a
        `LoopbackTask` for the Controller
        """
        client = mqtt.Client()
        client.enable_logger()  # type: ignore

        client.on_message = self._on_msg
        LOG.debug(f"MQTT Server: {MQTT_SERVER}/{MQTT_PORT}")
        client.connect(MQTT_SERVER, MQTT_PORT, 60)

        client.subscribe("sauna/+/loopback")
        LOG.info("Subscribed to sauna/+/loopback")
        client.on_disconnect = self._disconnect
        self._client = client
        self.append_task(
            LoopbackTask(
                delay=CONTROLLER_TIMEOUT,
                last_run=self._last_run,
                topic=MQTT_CONTROLLER_LOOPBACK_TOPIC,
            )
        )

        LOG.debug("MQTT setup done")
