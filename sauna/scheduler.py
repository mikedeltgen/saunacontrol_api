"""
Job Scheduler
"""

import asyncio
import logging
import time
from collections.abc import Callable
from dataclasses import dataclass, field
from threading import Thread
from typing import Any, List, TypeVar

LOG = logging.getLogger(__name__)

T = TypeVar("T")


@dataclass
class Job:
    name: str
    delay: float
    func: Callable[..., Any]
    params: Any
    last_run: float = field(default_factory=time.monotonic)
    run_once: bool = False


class Scheduler(Thread):
    """
    the scheduler runs jobs in a specified delay or just once
    """

    def __init__(self) -> None:
        super().__init__()
        self.jobs: List[Job] = []
        self.running = False
        self.scheduler_active = False
        self._loop = asyncio.get_event_loop()
        LOG.debug("Scheduler init")

    def run(self):
        LOG.info("Scheduler run")
        self.running = True
        self.scheduler_active = True
        while self.scheduler_active:
            time.sleep(0.01)
            job_kicker: List[Job] = []
            for job in self.jobs:
                now = time.monotonic()
                if now > job.last_run + job.delay:
                    LOG.debug(
                        (
                            f"Job : {job.name} "
                            f"last run: {job.last_run} -- {now - job.last_run}"
                        )
                    )
                    job.func(*job.params)
                    job.last_run = time.monotonic()
                    if job.run_once:
                        job_kicker.append(job)
            for job in job_kicker:
                del self.jobs[self.jobs.index(job)]
        self.running = False
        self.scheduler_active = False
        LOG.critical("Scheduler stopped")

    def remove_job(self, name: str):
        """
        remove job from job list
        """
        if not name:
            return
        job: List[Job] = [_ for _ in self.jobs if _.name == name]
        if job:
            for item in job:
                self.jobs.pop(self.jobs.index(item))
