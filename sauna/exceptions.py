"""
this module contains the exceptions for the sauna controller
"""


class SaunaException(Exception):
    def __init__(self, name: str = ""):
        if not name:
            name = self.__class__.__name__
        self.name = name


class CommunicationException(SaunaException):
    pass


class TimeOut(SaunaException):
    pass


class ControllerException(SaunaException):
    pass
