import time
from queue import Queue

from sauna.communication import get_communication_handler
from sauna.communication.objects import (
    ProtocolCommand,
)

q = Queue()

m = get_communication_handler(queue=q)
m.wait_for_start()

now = time.monotonic()

print(now)
while now + 15 > time.monotonic():
    m.receive()
    print("Receive queue: ", len(m.serial_objects))
print("-" * 200)

m.send(ProtocolCommand.SET_SAUNA_ACTIVE, bytearray([0x0E]))

time.sleep(1)
now = time.monotonic()
while now + 15 > time.monotonic():
    print("Receive queue: ", len(m.serial_objects))
print(m.serial_objects)
