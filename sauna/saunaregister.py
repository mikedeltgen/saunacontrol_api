"""
the SaunaRegister
"""
import logging
from collections import deque
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, List

from .sauna_controller.heater import HeaterElement

LOG = logging.getLogger(__name__)


def get_bit(data: bytearray, num: int) -> bool:
    base = int(num // 8)
    shift = int(num % 8)
    return bool((data[base] >> shift) & 0x1)


@dataclass
class DataPoint:
    current_temp: float
    pid_output: float
    set_point: float
    heaters: List[HeaterElement]


class SaunaRegister:
    def __init__(self) -> None:
        """
        initialize sauna register with default values
        """
        self.is_temperature: float = 0
        self.humidity: float = 0
        self._heater: List[HeaterElement] = []
        self.set_temperature: float = 0
        self.oven_temperature: float = 0
        self._lights: bytearray = bytearray([0, 0, 0, 0, 0, 0, 0, 0])
        self.pid_value: float = 0
        self.data_points: deque[DataPoint] = deque(maxlen=100)
        self.last_updated: datetime = datetime.min
        self.active: int = 0

    @property
    def heater(self) -> List[HeaterElement]:
        """
        the 3 heater states are  encoded in one byte this property
        converts the one byte value to tuple[bool,bool,bool]
        """
        return self._heater

    @heater.setter
    def heater(self, value: List[HeaterElement]):
        if self._heater != value:
            self._heater = value
            # self._update()

    @property
    def lights(self):
        return self._lights

    @lights.setter
    def lights(self, value: bytearray):
        if self._lights != value:
            self._lights = value

    def as_dict(self) -> Dict[str, Any]:
        """
        return SaunaRegister as dict
        """
        return dict(
            is_temperature=self.is_temperature,
            humidity=self.humidity,
            set_temperature=self.set_temperature,
            oven_temperature=self.oven_temperature,
            last_updated=self.last_updated,
            active=self.active,
        )

    def update(self, data: bytearray):
        """
        update SaunaRegister
        """
        self.is_temperature = data[0]
        self.humidity = data[1]
        self.set_temperature = data[3]
        self.oven_temperature = data[4]
        self.active = data[6]
        self.last_updated = datetime.now()
        LOG.debug(f"Register: {self.as_dict()}")
