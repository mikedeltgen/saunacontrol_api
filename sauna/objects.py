"""
objects used for sauna
"""
import time
from dataclasses import dataclass, field


@dataclass(unsafe_hash=True)
class LoopbackTask:
    delay: int
    topic: str
    last_run: int
    fail: bool = field(init=False, default=False)
    last_update: float = field(default=time.monotonic())
