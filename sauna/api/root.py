"""
API root route handler
"""


from typing import Dict

import pkg_resources


def root_handler() -> Dict[str, str]:
    """
    api root handler
    """
    version = pkg_resources.get_distribution("sauna").version
    pkg = pkg_resources.get_distribution("sauna").project_name
    return {"name": pkg, "version": version}
