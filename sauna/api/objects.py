from queue import Queue
from random import randint
from typing import Tuple

from sauna.monitor.main import Monitor
from sauna.sauna_controller.main import SaunaController
from sauna.scheduler import Scheduler

name = f"Controller {randint(1,20)}"

MONITOR_QUEUE: Queue[Tuple[str, str]] = Queue(maxsize=20)
SAUNA_CONTROLLER = SaunaController()
SCHEDULER = Scheduler()
MONITOR: Monitor = Monitor(queue=MONITOR_QUEUE)
