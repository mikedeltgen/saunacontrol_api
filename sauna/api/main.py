"""
Fast API for SAUNA Control
"""
import asyncio
import logging
import time
from contextlib import asynccontextmanager
from datetime import datetime, timedelta
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path

from fastapi import FastAPI, Request, WebSocket
from fastapi.encoders import jsonable_encoder

# from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse, JSONResponse
from gouge.colourcli import Simple

from sauna import __version__
from sauna.api.objects import (
    MONITOR,
    MONITOR_QUEUE,
    SAUNA_CONTROLLER,
    SCHEDULER,
)
from sauna.environment import (
    API_LOOPBACK_TIMEOUT,
    CONTROLLER_WATCHDOG_TIMEOUT,
    CONTROLLER_WATCHDOG_TOPIC,
    LOG_FILE,
    LOGGING_LEVEL,
    MQTT_API_LOOPBACK_TOPIC,
    SAUNA_MAX_TIME,
)
from sauna.exceptions import CommunicationException, ControllerException
from sauna.objects import LoopbackTask
from sauna.scheduler import Job

HERE = Path(__file__).parent

P_LOG_FILE = Path(LOG_FILE)
P_LOG_FILE.parent.mkdir(exist_ok=True)

LOG = logging.getLogger()


def setup_logging():
    LOG.setLevel(level=LOGGING_LEVEL)
    handler = TimedRotatingFileHandler(
        P_LOG_FILE, when="midnight", backupCount=3
    )
    handler.setFormatter(Simple())
    handler.setLevel(level=LOGGING_LEVEL)
    LOG.addHandler(handler)
    mqtt = logging.getLogger("paho.mqtt.client")
    mqtt.setLevel(logging.INFO)

    handler = logging.StreamHandler()
    handler.setFormatter(Simple())
    handler.setLevel(level=LOGGING_LEVEL)
    LOG.addHandler(handler)


def api_loopback():
    """
    this function sens an datetime message to the API Loopback MQTT Topic
    """
    MONITOR_QUEUE.put(
        (
            MQTT_API_LOOPBACK_TOPIC,
            datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
    )
    LOG.debug("Send Loopback message")


def controller_watchdog():
    """
    check if the sauna register is updated in regular intervals and report this
    to the MQTT Topic of the Controller Watchdog
    """
    max = datetime.now() - timedelta(seconds=CONTROLLER_WATCHDOG_TIMEOUT)
    if SAUNA_CONTROLLER.sauna_register.last_updated > max:
        MONITOR_QUEUE.put(
            (
                CONTROLLER_WATCHDOG_TOPIC,
                "running",
            )
        )
    else:
        MONITOR_QUEUE.put(
            (
                CONTROLLER_WATCHDOG_TOPIC,
                "no data",
            )
        )
        LOG.critical("Controller watchdog: No Response from µController")
        SAUNA_CONTROLLER.stop_all()


def sauna_controller_watchdog():
    """
    check if the sauna SAUNA_CONTROLLER is still alive
    """
    if not SAUNA_CONTROLLER.serial_thread.is_alive():
        LOG.critical("Sauna Controller Watchdog restarting Sauna Controller")
        SAUNA_CONTROLLER.stop_all()


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    this method is executed on FastAPI start and runs until the FastAPI is
    stopped
    """
    SAUNA_CONTROLLER.setup()
    SAUNA_CONTROLLER.serial_operational.wait()
    SAUNA_CONTROLLER.serial_exception.wait(1)
    if SAUNA_CONTROLLER.serial_exception.is_set():
        raise ConnectionError("Sauna Controller Exception")

    MONITOR.setup()
    MONITOR.start()
    MONITOR.append_task(
        LoopbackTask(
            delay=API_LOOPBACK_TIMEOUT,
            last_run=int(time.monotonic()),
            topic=MQTT_API_LOOPBACK_TOPIC,
        )
    )

    SCHEDULER.jobs.append(
        Job(
            name="API Loopback",
            delay=API_LOOPBACK_TIMEOUT,
            func=api_loopback,
            params=(),
        )
    )
    SCHEDULER.jobs.append(
        Job(
            name="Controller Watchdog",
            delay=CONTROLLER_WATCHDOG_TIMEOUT,
            func=controller_watchdog,
            params=(),
        )
    )
    SCHEDULER.jobs.append(
        Job(
            name="Sauna Controller Watchdog",
            delay=60,
            func=sauna_controller_watchdog,
            params=(),
        )
    )
    SCHEDULER.start()
    yield
    SAUNA_CONTROLLER.stop_all()
    MONITOR.stopped = True
    SCHEDULER.scheduler_active = False
    SCHEDULER.join()


APP = FastAPI(lifespan=lifespan)
# APP.add_middleware(
#     CORSMiddleware,
#     allow_origins=["*"],
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )

# @APP.exception_handler(ControllerException)
# def controller_exception_handler(  # type: ignore
#     request: Request, exc: ControllerException
# ):
#     LOG.critical("Controller Exception")
#     SAUNA_CONTROLLER.controller_stop = True
#     SAUNA_CONTROLLER.join()
#     SAUNA_CONTROLLER.setup()
#     SAUNA_CONTROLLER.start()
#     LOG.warning("Sauna Controller restarted.")
#     return JSONResponse(
#         status_code=500, content={"message": "Sauna Controller failed!"}
#     )


@APP.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        await asyncio.sleep(2)
        data = {}
        data["sauna_register"] = jsonable_encoder(
            SAUNA_CONTROLLER.sauna_register.as_dict()
        )

        data["state"] = "off,off,off"
        data["pid_value"] = str(SAUNA_CONTROLLER.pid)
        data["time_left"] = str(SAUNA_CONTROLLER.sauna_rest_time)
        await websocket.send_json(data)


@APP.get("/heater")
async def status_heater_states():
    """
    get states of the heaters
    """
    return SAUNA_CONTROLLER.heater.as_dict()


# TODO: could be one route returning everything related to the monitor
@APP.get("/monitor/tasks")
async def get_monitor_tasks():
    return MONITOR.list_tasks()


@APP.get("/sauna")
async def sauna_status():
    """
    return sauna status
    the state is modified to start,stop for openhab
    """
    if SAUNA_CONTROLLER.session_active.is_set():
        state = "start"
    else:
        state = "stop"

    return {
        "time_left": SAUNA_CONTROLLER.sauna_rest_time,
        "state": state,
    }


@APP.put("/sauna/start")
async def sauna_start():
    """
    start the sauna
    """
    if not SAUNA_CONTROLLER.session_active.is_set():
        SCHEDULER.jobs.append(
            Job(
                name="Sauna max time",
                delay=SAUNA_MAX_TIME,
                func=SAUNA_CONTROLLER.stop_session,
                params=([]),
                run_once=True,
            )
        )
        # TODO: eventually add some more time to the delay (clash deactivate)
        SCHEDULER.jobs.append(
            Job(
                name="Sauna Main Switch",
                delay=SAUNA_MAX_TIME,
                func=SAUNA_CONTROLLER.heater.main_switch,
                params=([False]),
                run_once=True,
            )
        )
        SCHEDULER.jobs.append(
            Job(
                name="Sauna rest time emitter",
                delay=60,
                func=SAUNA_CONTROLLER.send_session_rest_time,
                params=([]),
            )
        )

        SAUNA_CONTROLLER.start_session()
        LOG.debug("Waiting for sauna session to start")
        SAUNA_CONTROLLER.session_active.wait()
    return {
        "time_left": SAUNA_CONTROLLER.sauna_rest_time,
        "state": "start",
    }


# TODO: take care of removing the SCHEDULER TASKS
@APP.put("/sauna/stop")
async def sauna_stop():
    """
    stop the sauna
    """
    SAUNA_CONTROLLER.stop_session()
    SCHEDULER.remove_job(name="Sauna rest time emitter")
    return {
        "time_left": SAUNA_CONTROLLER.sauna_rest_time,
        "state": "stop",
    }


# TODO: could be returned by /sauna
@APP.get("/sauna/humidity")
async def get_sauna_humidity():
    """
    get current sauna humidity
    """
    return {
        "humidity": SAUNA_CONTROLLER.sauna_register.humidity,
    }


# TODO: could be returned by /sauna
@APP.get("/sauna/register")
async def get_sauna_register():
    """
    get current sauna register
    """
    return SAUNA_CONTROLLER.sauna_register.as_dict()


# TODO: could be returned by /sauna
@APP.get("/sauna/temperature")
async def get_sauna_temperature():
    """
    get current sauna temperature
    """
    return {
        "temperature": SAUNA_CONTROLLER.sauna_register.is_temperature,
    }


@APP.put("/sauna/temperature/{temperature}")
async def set_sauna_temperature(
    temperature: int,
):
    """
    set the sauna set temperature
    """
    SAUNA_CONTROLLER.set_setpoint_temperature(temperature)
    SAUNA_CONTROLLER.response_received.wait()
    return {"set_temperature": SAUNA_CONTROLLER.set_point}


@APP.get("/favicon.ico", include_in_schema=False)
async def favicon():
    favicon = HERE / "static" / "favicon.png"
    return FileResponse(favicon)


# @APP.get("/")
# async def root():
#     """
#     API Root
#     """
#     return root_handler()


setup_logging()
LOG.info(f"Sauna Control API version {__version__}")
