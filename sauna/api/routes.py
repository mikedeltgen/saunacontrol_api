import asyncio
import logging
from pathlib import Path

from fastapi import APIRouter, WebSocket
from fastapi.encoders import jsonable_encoder
from fastapi.responses import FileResponse

# from fastapi.responses import FileResponse
from sauna.api.objects import MONITOR, SAUNA_CONTROLLER, SCHEDULER
from sauna.monitor.main import Monitor
from sauna.sauna_controller.main import SaunaController

from ..environment import (
    SAUNA_MAX_TIME,
)
from ..scheduler import Job, Scheduler
from . import root_handler

LOG = logging.getLogger()
HERE = Path(__file__).parent

router = APIRouter()


@router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        await asyncio.sleep(2)
        data = {}
        data["sauna_register"] = jsonable_encoder(
            SAUNA_CONTROLLER.sauna_register.as_dict()
        )

        data["state"] = "off,off,off"
        data["pid_value"] = str(SAUNA_CONTROLLER.pid)
        data["time_left"] = str(SAUNA_CONTROLLER.sauna_rest_time)
        await websocket.send_json(data)


# TODO: change this to /heater
@router.get("heater")
async def status_heater_states(controller: SaunaController):
    """
    get states of the heaters
    """
    return controller.heater.as_dict()


# TODO: could be one route returning everything related to the monitor
@router.get("/monitor/tasks")
async def get_monitor_tasks(monitor: Monitor):
    return monitor.list_tasks()


@router.get("/sauna")
async def sauna_status(controller: SaunaController):
    """
    return sauna status
    the state is modified to start,stop for openhab
    """
    if controller.session_active.is_set():
        state = "start"
    else:
        state = "stop"

    return {
        "time_left": controller.sauna_rest_time,
        "state": state,
    }


@router.put("/sauna/start")
async def sauna_start(controller: SaunaController, scheduler: Scheduler):
    """
    start the sauna
    """
    if not controller.session_active.is_set():
        scheduler.jobs.append(
            Job(
                name="Sauna max time",
                delay=SAUNA_MAX_TIME,
                func=controller.stop_session,
                params=([]),
                run_once=True,
            )
        )
        # TODO: eventually add some more time to the delay (clash deactivate)
        scheduler.jobs.append(
            Job(
                name="Sauna Main Switch",
                delay=SAUNA_MAX_TIME,
                func=controller.heater.main_switch,
                params=([False]),
                run_once=True,
            )
        )
        scheduler.jobs.append(
            Job(
                name="Sauna rest time emitter",
                delay=60,
                func=controller.send_session_rest_time,
                params=([]),
            )
        )

        controller.start_session()
        LOG.debug("Waiting for sauna session to start")
        controller.session_active.wait()
    return {
        "time_left": controller.sauna_rest_time,
        "state": "start",
    }


# TODO: take care of removing the SCHEDULER TASKS
@router.put("/sauna/stop")
async def sauna_stop(controller: SaunaController, scheduler: Scheduler):
    """
    stop the sauna
    """
    controller.stop_session()
    scheduler.remove_job(name="Sauna rest time emitter")
    return {
        "time_left": controller.sauna_rest_time,
        "state": "stop",
    }


# TODO: could be returned by /sauna
@router.get("/sauna/humidity")
async def get_sauna_humidity(controller: SaunaController):
    """
    get current sauna humidity
    """
    return {
        "humidity": controller.sauna_register.humidity,
    }


# TODO: could be returned by /sauna
@router.get("/sauna/register")
async def get_sauna_register(controller: SaunaController):
    """
    get current sauna register
    """
    return controller.sauna_register.as_dict()


# TODO: could be returned by /sauna
@router.get("/sauna/temperature")
async def get_sauna_temperature(controller: SaunaController):
    """
    get current sauna temperature
    """
    return {
        "temperature": controller.sauna_register.is_temperature,
    }


@router.put("/sauna/temperature/{temperature}")
async def set_sauna_temperature(temperature: int, controller: SaunaController):
    """
    set the sauna set temperature
    """
    controller.set_setpoint_temperature(temperature)
    controller.response_received.wait()
    return {"set_temperature": controller.set_point}


@router.get("/favicon.ico", include_in_schema=False)
async def favicon():
    favicon = HERE / "static" / "favicon.png"
    return FileResponse(favicon)


@router.get("/")
async def root():
    """
    API Root
    """
    return root_handler()
