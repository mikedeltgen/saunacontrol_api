
api start
    - init serial port
    - wait for serial data reception
    - continue to parse the received data in background

api run

    - get data from µC
    - generate loopback messages
    - check loopback messages

    - handle api calls

