"""
CRC implementation
"""


class CRC:
    def __init__(self) -> None:
        self.value: int = 0x0000

    def calculate(self, data: bytearray, initial: int = 0xFFFF) -> int:
        """
        calculate crc
        """
        crc = initial
        for byte in data:
            # print(f"byte: {byte}")
            crc ^= byte << 8
            for _ in range(8):
                if crc & 0x8000:
                    crc = (crc << 1) ^ 0x1021
                else:
                    crc <<= 1

        self.value = crc
        return crc

    def to_bytearray(self) -> bytearray:
        return bytearray([(self.value >> 8) & 0xFF, self.value & 0xFF])


if __name__ == "__main__":
    out = bytearray(b"SA:\x06\xa9\xfc\xff\xd5\x01\x01;:\x01;\x01")
    inp = bytearray(b"SA:\x01;\x01")
    c = CRC()
    c.calculate(inp, 0xFFFF)
    print(f"Value: {c.to_bytearray()}")
    v = " ".join([hex(_) for _ in inp])
    print(f"Src: {v}")
