"""
sauna controller
"""

import logging
import sys
import time
from collections import deque
from datetime import datetime, timedelta
from threading import Event, Thread, currentThread
from typing import Dict, List, Tuple

from serial import SerialException
from simple_pid import PID

from sauna.exceptions import CommunicationException

from ..crc import CRC
from ..environment import (
    CONTROLLER_TIMEOUT,
    CONTROLLER_VERSION,
    HEATER1_PIN,
    HEATER2_PIN,
    HEATER3_PIN,
    MAIN_SWITCH_PIN,
    PID_KD,
    PID_KI,
    PID_KP,
    SAUNA_MAX_TIME,
    SAUNA_PID_SAMPLE_TIME,
)
from ..saunaregister import SaunaRegister
from .heater import Heater
from .objects import (
    BOOT_ID,
    BOT,
    CONTROLLER_AUTO_SEND,
    DELIMITER,
    EOT,
    EncodeError,
    ProtocolCommand,
    ReceiveObject,
    SerialCom,
)

LOG = logging.getLogger(__name__)


class SaunaController:
    def __init__(self) -> None:
        self.sauna_register: SaunaRegister = SaunaRegister()
        self.heater: Heater
        self.pid_values: Tuple[float, float, float]
        self.pid: PID
        self.sample_time: float
        self._sauna_off_time = datetime.max
        self.mainThread = currentThread()
        self.set_point: float = 0
        self.last_set_point: float = 0
        # serial stuff
        self.serial: SerialCom = SerialCom()
        self._receive_buffer: bytearray = bytearray()
        self.serial_thread = Thread(target=self.serial_reader)
        self.ids: deque[int] = deque([_ for _ in range(1, 128)])
        self.received_objects: Dict[int, ReceiveObject] = {}
        self.controller_watchdog = Thread()
        # Events
        self.decode_failed: Event = Event()
        self.decode_failed.clear()
        self.session_stopped: Event = Event()
        self.session_stopped.set()
        self.session_active: Event = Event()
        self.session_active.clear()
        self.session_rest_time_event: Event = Event()
        self.serial_operational: Event = Event()
        self.serial_operational.clear()
        self.serial_exception: Event = Event()
        self.serial_exception.clear()
        self.serial_reader_running: Event = Event()
        self.serial_reader_running.clear()
        self.serial_running = Event()
        self.serial_running.clear()
        self.clean_up: Event = Event()
        self.clean_up.clear()
        self.response_received: Event = Event()
        self.response_received.clear()
        self.ready_to_send: Event = Event()
        self.ready_to_send.set()

    @property
    def sauna_rest_time(self) -> int:
        now = datetime.now()
        if self._sauna_off_time == datetime.max:
            ret = 0
        elif self._sauna_off_time >= now:
            delta = (self._sauna_off_time - now).seconds
            ret = int(delta / 60)
            if delta < 60:
                ret = delta
            if ret > 255:
                ret = 255

            return ret
        return 0

    def setup(
        self,
        set_point: float = 25.0,
        sample_time: float = SAUNA_PID_SAMPLE_TIME,
        pid_values: Tuple[float, float, float] = (PID_KP, PID_KI, PID_KD),
    ) -> None:
        """
        set initial `set_point` value default 25, set initial PID sample time
        (default 1s). set initial PID settings.
        initialize Serial Port.
        initialize SerialProto.
        """
        self.heater = Heater(
            HEATER1_PIN, HEATER2_PIN, HEATER3_PIN, MAIN_SWITCH_PIN
        )
        self.set_point = set_point
        self.sample_time = sample_time
        self.pid_values = pid_values
        self.pid = PID(
            self.pid_values[0],
            self.pid_values[1],
            self.pid_values[2],
            setpoint=self.set_point,
            output_limits=(0, 100),
            proportional_on_measurement=True,
            sample_time=self.sample_time,
        )
        # self.pid.proportional_on_measurement = True
        self.serial_thread.start()
        _ = Thread(target=self.cleanup_receive_objects)
        _.start()
        LOG.info(
            "[SETUP] "
            f"PID: Setpoint-{self.set_point} Kd-{self.pid.Kd} "
            f"Ki-{self.pid.Ki} Kp-{self.pid.Kp}"
        )
        LOG.info("Sauna Controller Setup done.")

    def cleanup_receive_objects(self):
        """
        cleanup old receive objects
        """
        if not self.clean_up.wait(10):
            ids: List[int] = []
            for id, obj in self.received_objects.items():
                if id == CONTROLLER_AUTO_SEND:
                    continue
                if obj.received + timedelta(seconds=20) < datetime.now():
                    ids.append(id)
            if ids:
                LOG.debug(f"Cleaning up. {len(ids)}")
            for id in ids:
                del self.received_objects[id]
            if not self.mainThread.is_alive():
                LOG.info("Cleanup exited")
                sys.exit(1)
            _ = Thread(target=self.cleanup_receive_objects)
            _.start()

    def send_session_rest_time(self):
        LOG.debug(f"Sending rest time: {self.sauna_rest_time}")
        if not self.serial_operational.is_set():
            self.session_stopped.set()
            return
        try:
            ret = self.send(
                ProtocolCommand.SET_SAUNA_ACTIVE,
                bytearray([self.sauna_rest_time]),
            )
            if not self.session_rest_time_event.wait(10) and ret > 0:
                if self.session_active.is_set():
                    LOG.debug("Next rest time will be send in: 10")
                    # _ = Thread(target=self.send_session_rest_time)
                    # _.start()
        except CommunicationException as e:
            LOG.exception("Communication Exception stopping Session")
            LOG.debug(f"Exception: {e}")
            self.serial_exception.set()
            self.session_stopped.set()

    def start_session(self):
        """
        start a sauna session
        """
        LOG.info("Starting Session")
        self.serial_operational.wait()
        self.session_stopped.clear()
        self.session = Thread(target=self.sauna_session)

        # start session timer
        session_max_time = Thread(target=self.session_max_time)
        # start display updated
        # _display = Thread(target=self.send_session_rest_time)
        self.send_session_rest_time()
        session_max_time.start()
        self.session.start()

    def session_max_time(self):
        """
        wait until SAUNA_MAX_TIME is reached and then stop the session
        """
        LOG.warning(f"Session will be stopped in {SAUNA_MAX_TIME}")
        if not self.session_stopped.wait(SAUNA_MAX_TIME):
            LOG.info("Session stopped due to MAX TIME")
            self.session_stopped.set()

    def stop_session(self):
        """
        set the sauna stop event witch stops the current sauna session
        """
        self._sauna_off_time = datetime.max
        self.session_stopped.set()

    def sauna_session(self):
        """
        the actual sauna session thread
        """
        # self.pid.reset()
        self.session_active.set()
        self._sauna_off_time = datetime.now() + timedelta(
            seconds=SAUNA_MAX_TIME
        )
        LOG.info(f"Sauna Session running until {self._sauna_off_time}")
        self.pid.reset()
        self.pid.setpoint = self.set_point
        self.pid_sample(current_temp=self.sauna_register.is_temperature)
        LOG.info(
            "[SESSION] "
            f"PID: Setpoint-{self.set_point} Kd-{self.pid.Kd} "
            f"Ki-{self.pid.Ki} Kp-{self.pid.Kp}"
        )
        self.heater.main_switch(True)
        while not self.session_stopped.is_set():
            # do the stuff needed for active sauna session
            self.pid_sample(current_temp=self.sauna_register.is_temperature)
            time.sleep(1)
            pass
        # turn off Heaters and Main Switch
        self.control_heaters(0)
        self.heater.engage()
        self.heater.main_switch(False)
        self.session_active.clear()
        # Send SA to µC
        LOG.info("Waiting for Rest Time emitter to finish")

        self.send(
            ProtocolCommand.SET_SAUNA_ACTIVE,
            bytearray([0x00]),
        )
        self.send(
            ProtocolCommand.SET_HEATER_STATE,
            self.heater.to_byte(),
        )
        LOG.info("Sauna Session Stopped")

    def pid_sample(self, current_temp: float):
        """
        calculate a new PID value based on the current temperature.
        #TODO: implement security system that checks if the value of current
                temperature does not change over some time(µC problem, com prob)
        """
        result = self.pid(current_temp)
        if not result:
            result = 0
        self.control_heaters(result)
        LOG.debug(
            f"PID Output: {int(result)} - SetPoint: {self.pid.setpoint}"
            f" - Current: {current_temp}"
        )
        if self.heater.engage():
            try:
                # wait here
                self.send(
                    ProtocolCommand.SET_HEATER_STATE,
                    self.heater.to_byte(),
                )
            except CommunicationException as e:
                LOG.exception("Communication Exception stopping Session")
                LOG.debug(f"Exception: {e}")
                self.session_stopped.set()

    def control_heaters(self, result: float):
        """
        sample a new PID value and engage the heaters
        removed the if else statements because the SSR blew up and needs to be
        replaced by a standard relay.
        #TODO: find a better name
        """
        if not result:
            result = 0
        result = round(result, 2)
        heater_multiplier = 0
        if result >= 51:
            heater_multiplier = 3
        self.heater.round_robin(heater_multiplier)

    def set_setpoint_temperature(self, value: int):
        """
        set a new setpoint temperature
        """
        if value == 0.0:
            return
        self.set_point = value
        self.pid.setpoint = value
        # self.pid.reset()
        LOG.debug(f"Setting Setpoint to: {bytearray([int(value)])}")
        try:
            self.send(ProtocolCommand.SET_SAUNA_TEMP, bytearray([int(value)]))
        except CommunicationException as e:
            LOG.exception("Communication Exception stopping Session")
            LOG.debug(f"Exception: {e}")
            self.session_stopped.set()

    def update_setpoint(self):
        value = self.sauna_register.set_temperature
        if value != self.last_set_point:
            LOG.info("Setpoint updated")
            self.last_set_point = value
            self.set_point = value
            self.pid.setpoint = value
            self.pid = PID(
                self.pid_values[0],
                self.pid_values[1],
                self.pid_values[2],
                setpoint=self.set_point,
                output_limits=(0, 100),
                sample_time=self.sample_time,
            )

            LOG.info(
                "[UPDATE] "
                f"PID: Setpoint-{self.set_point} Kd-{self.pid.Kd} "
                f"Ki-{self.pid.Ki} Kp-{self.pid.Kp}"
            )

    def update_sauna_register(self):
        if (
            CONTROLLER_AUTO_SEND in self.received_objects
            and self.serial_operational.is_set()
        ):
            LOG.debug("Sauna Register updated.")
            item = self.received_objects[CONTROLLER_AUTO_SEND]
            self.sauna_register.update(item.data)
            self.update_setpoint()
            if self.sauna_register.set_temperature != self.set_point:
                self.set_point = int(self.sauna_register.set_temperature)
            del self.received_objects[CONTROLLER_AUTO_SEND]

    # Serial Stuff

    def packet_builder(self) -> bytearray:
        """
        build packet from receive buffer
        """
        ret = bytearray()
        if BOT in self._receive_buffer and EOT in self._receive_buffer:
            begin = self._receive_buffer.index(BOT)
            end = self._receive_buffer.index(EOT)
        else:
            return ret
        ret = self._receive_buffer[begin : end + 3]
        self._receive_buffer = self._receive_buffer[end + 3 :]
        return ret

    def check_controller_version(self):
        """
        check if controller is in a compatible version
        """
        if BOOT_ID in self.received_objects:
            obj = self.received_objects[BOOT_ID]
            v_pos = obj.data.index(b"v")
            version = "undefined"
            if v_pos > 0:
                version = obj.data[v_pos + 2 : -1].decode("utf-8")
            if version != CONTROLLER_VERSION:
                self.serial_exception.set()
                LOG.critical(
                    (
                        f"Wrong controller version. Expected: {CONTROLLER_VERSION}."
                        f"Received: {version}"
                    )
                )

    def wait_for_controller(self):
        """
        wait for controller to boot or an other auto send message
        """
        if self.serial_operational.is_set():
            return
        if BOOT_ID in self.received_objects:
            self.check_controller_version()
            self.serial_operational.set()
            LOG.info("Got Boot message from µController")
        elif CONTROLLER_AUTO_SEND in self.received_objects:
            del self.received_objects[CONTROLLER_AUTO_SEND]
            crc = CRC()
            buf = bytearray()
            buf += ProtocolCommand.OPERATIONAL.value
            buf += DELIMITER
            buf += bytearray([0x01])
            buf += bytearray([0x01])
            buf += bytearray([BOOT_ID])
            crc.calculate(buf)
            buf = BOT + buf + crc.to_bytearray() + EOT
            self.serial_send(buf)
            LOG.info("Waiting for controller to boot")

    def serial_reader(self):
        """
        read data from serial port and build data packets
        """
        self.serial.setup()

        LOG.info("Starting Serial Reader")
        self.serial_operational.clear()
        self.serial_reader_running.set()
        while self.serial_reader_running.is_set():
            try:
                data = self.serial.read(1)
            except (SerialException, CommunicationException) as e:
                data = bytearray()
                LOG.exception("Serial Exception occurred")
                LOG.debug(f"Exception: {e}")
                self.serial_exception.set()
                self.stop_all()
            ret = self.packet_builder()

            if ret:
                try:
                    obj = ReceiveObject.encode(ret)
                    self.received_objects[obj.id] = obj
                    self.wait_for_controller()
                except EncodeError:
                    self.decode_failed.set()
                    LOG.warning(f"Could not decode Object. {ret}")
            if data:
                self._receive_buffer.extend([_ for _ in data])
            self.update_sauna_register()
            if not self.mainThread.is_alive():
                LOG.info("Serial Reader exited")
                self.session_stopped.set()
                self.serial_reader_running.clear()
                self.serial_operational.clear()
        self.stop_all()
        LOG.critical("Serial Reader Stopped")

    def serial_send(self, data: bytearray):
        """
        write data to serial
        """
        self.serial.write(data)

    def send(self, command: ProtocolCommand, data: bytearray) -> int:
        """
        send a command to the µController
        """
        self.ready_to_send.wait()

        if not self.serial_operational.is_set():
            LOG.warning("Serial not ready")
            return -1
        self.ready_to_send.clear()
        id = self.ids.popleft()
        crc = CRC()
        buf = bytearray()
        buf += command.value
        buf += DELIMITER
        buf += bytearray([len(data)])
        buf += data
        buf += bytearray([id])
        crc.calculate(buf)
        buf = BOT + buf + crc.to_bytearray() + EOT
        self.serial_send(buf)
        LOG.debug(
            f"Sending Command `{command.value}` with id: `{bytearray([id])}"
        )
        self.response_received.clear()
        time_out = time.monotonic() + CONTROLLER_TIMEOUT
        while not self.response_received.is_set():
            if not self.serial_operational.is_set():
                raise CommunicationException("Serial not operational")
            if id in self.received_objects or self.decode_failed.is_set():
                self.response_received.set()
            if time_out < time.monotonic():
                raise CommunicationException(
                    f"No Response after Time out for command: {command!s}"
                )
            time.sleep(0.5)

        if self.decode_failed.is_set():
            self.decode_failed.clear()
            # LOG.warning("Resending Command due to decode error")
            # self.send(command=command, data=data)

        if id in self.received_objects:
            if self.received_objects[id].command == ProtocolCommand.ERROR:
                LOG.warning(
                    f"Protocol Command Error. {self.received_objects[id]!r}"
                )
            else:
                LOG.debug(f"Command: {command!s} send successful.")
            del self.received_objects[id]
            self.ids.append(id)
            self.ready_to_send.set()
            return id
        return -1

    def stop_all(self):
        self.heater.stop()
        self.session_stopped.set()
        self.session_active.clear()
        self.decode_failed.set()
        self.serial_operational.clear()
        self.serial_reader_running.clear()
        self.serial_running.clear()
        self.clean_up.clear()
        # sys.exit(126)
