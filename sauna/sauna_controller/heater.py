"""
Heater module
"""

import json
import logging
from dataclasses import asdict, dataclass, field
from enum import Enum
from itertools import cycle
from typing import Dict, List, Optional, Set, Tuple

try:
    import RPi.GPIO as GPIO  # type:ignore
except (ModuleNotFoundError, ImportError):
    from ..alt_gpio import GPIO

LOG = logging.getLogger(__name__)


class Heater_Name(Enum):
    H1 = "Heater 1"
    H2 = "Heater 2"
    H3 = "Heater 3"
    UNKNOWN = "unknown"
    UNDEFINED = "undefined"


@dataclass(unsafe_hash=True)
class HeaterElement:
    name: Heater_Name
    pin: int
    status: bool = field(init=False, default=False)

    def get_status(self) -> bool:
        """
        get current heater status
        """
        # implement function to return the current state of the GPIO pin

        return self.status

    def on(self) -> bool:
        """
        turn heater element on
        """
        if self.status:
            return self.status
        LOG.debug(f"Heater Element {self.name} turned ON")
        self.status = True
        GPIO.output(self.pin, True)  # type:ignore
        return self.status

    def off(self) -> bool:
        """
        turn heater element off
        """
        if not self.status:
            return self.status
        LOG.debug(f"Heater Element {self.name} turned OFF")
        self.status = False
        GPIO.output(self.pin, False)  # type:ignore
        return self.status


class Heater:
    """
    Heater Class
    """

    def __init__(
        self, h1_pin: int, h2_pin: int, h3_pin: int, killswitch_pin: int
    ) -> None:
        self.pins: Tuple[int, int, int] = (h1_pin, h2_pin, h3_pin)
        self.killswitch_pin = killswitch_pin
        self.heaters: List[HeaterElement] = [
            HeaterElement(Heater_Name.H1, self.pins[0]),
            HeaterElement(Heater_Name.H2, self.pins[1]),
            HeaterElement(Heater_Name.H3, self.pins[2]),
        ]
        self.next_heater: cycle[HeaterElement] = cycle(self.heaters)
        self.active_heaters: List[HeaterElement] = []
        GPIO.cleanup()  # type:ignore
        GPIO.setmode(GPIO.BOARD)  # type:ignore
        GPIO.setup(h1_pin, GPIO.OUT)  # type:ignore
        GPIO.setup(h2_pin, GPIO.OUT)  # type:ignore
        GPIO.setup(h3_pin, GPIO.OUT)  # type:ignore
        GPIO.setup(killswitch_pin, GPIO.OUT)  # type:ignore

    def main_switch(self, enabled: bool = False):
        GPIO.output(self.killswitch_pin, enabled)  # type:ignore
        LOG.warning(
            f"Killswitch on pin {self.killswitch_pin} set to: {enabled}"
        )

    def next_not_active_heater(self) -> Optional[HeaterElement]:
        """
        get next Heater Element not currently active
        """
        for _ in range(len(self.heaters)):
            elem = next(self.next_heater)
            if elem not in self.active_heaters:
                return elem
        return None

    def round_robin(self, multiplier: int):
        """
        round robin for heater's
        """
        if multiplier == 0:
            self.active_heaters = []
            return
        active_count = len(self.active_heaters)
        if active_count == multiplier:
            return
        if active_count < multiplier:
            for _ in range(multiplier):
                heater = self.next_not_active_heater()
                if heater:
                    self.active_heaters.append(heater)
            return
        if active_count > multiplier:
            for _ in range(active_count - multiplier):
                self.active_heaters.pop()

    def engage(self) -> bool:
        _heaters = [_.status for _ in self.heaters]
        off_heaters: Set[HeaterElement] = set(self.heaters).difference(
            set(self.active_heaters)
        )
        for _ in off_heaters:
            _.off()
        for _ in self.active_heaters:
            _.on()
        return _heaters != [_.status for _ in self.heaters]

    def off(self):
        """
        turn all heater elements off
        """
        for _ in self.heaters:
            _.off()

    def as_dict(self) -> List[Dict[str, str]]:
        """
        return Heater as dict
        """
        return [asdict(_) for _ in self.heaters]

    def as_json(self) -> str:
        return json.dumps(self.as_dict())

    def to_byte(self) -> bytearray:
        """
        convert current heater state to byte(array)
        """
        res = 0
        for idx, heater in enumerate(self.heaters):
            res = res | (heater.status << idx)
        return bytearray([res])

    def stop(self):
        """
        stop all heater elements
        """
        self.off()
        GPIO.cleanup()  # type:ignore
