"""
protocol objects
"""

import logging
import time
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from threading import Event, Thread
from typing import Any, Optional

from serial import Serial
from serial.serialutil import SerialException

from sauna.exceptions import CommunicationException

from ..environment import SERIAL_PORT

LOG = logging.getLogger(__name__)

# this 3 values could be gathered from the config/Environment
BOT = bytearray([0xAA, 0xBB, 0xCC])
EOT = bytearray([0xDD, 0xEE, 0xFF])
DELIMITER = bytearray(b":")

BOOT_ID = 0x82
CONTROLLER_AUTO_SEND = 0xAA


class CRCError(Exception):
    pass


class EncodeError(Exception):
    pass


class ProtocolCommand(Enum):
    GET = bytearray(b"GET")
    GET_SAUNA_REGISTER = bytearray(b"GR")
    SET_SAUNA_REGISTER = bytearray(b"SR")
    SET_SAUNA_TEMP = bytearray(b"ST")
    SET_HEATER_STATE = bytearray(b"SH")
    SET_SAUNA_ACTIVE = bytearray(b"SA")
    OPERATIONAL = bytearray(b"OP")

    RESP = bytearray(b"RESP")
    REG = bytearray(b"REG")
    BA = bytearray(b"Ba")
    ERROR = bytearray(b"ERR")
    ACK = bytearray(b"ACK")
    RUN = bytearray(b"RUN")
    NOP = bytearray(b"NOP")


class State(Enum):
    INIT = 0
    WAITING_FOR_START = 10
    WAITING_FOR_END = 50
    ERROR = 999


def empty_bytearray():
    return bytearray(b"")


@dataclass
class ReceiveObject:
    """
    Sauna Protocol Object
    :param received: the datetime when the object was created
    :param data: the received data bytearray
    :param command: the received `Command`
    :param id: the id corresponding to a `SendObject.id` or (int)0 if no
               related `SendObject`
    """

    received: datetime = field(default_factory=datetime.now)
    data: bytearray = field(default_factory=empty_bytearray)
    command: ProtocolCommand = field(default=ProtocolCommand.NOP)
    id: int = field(default=0)

    @classmethod
    def encode(cls, raw_data: bytearray) -> "ReceiveObject":
        """
        encode given data and return  ReceiveObject or None if not encodable
        """

        # crc = CRC()
        data = raw_data.removeprefix(BOT)
        data = data.removesuffix(EOT)
        cmd, _delimiter, data = data.partition(DELIMITER)
        if not data:
            raise EncodeError(f"Delimiter `{DELIMITER}` not found.")
        data_length = data.pop(0)
        payload = data[:data_length]
        del data[:data_length]
        if not data:
            raise EncodeError(f"No Command ID found. {raw_data}")
        cmd_id = data.pop(0)
        try:
            command = ProtocolCommand(cmd)
        except ValueError:
            raise EncodeError(f"`{cmd}` is not a valid Command")
        return ReceiveObject(data=payload, command=command, id=cmd_id)


@dataclass
class SendObject:
    """
    :param emitted: the datetime when the object was send to µC
    :param queued: the datetime when the object was put on the queue
    :param data: the bytearray to send
    :param command: the `Command` to send
    :param id: the expected id to be returned with a corresponding
               `ReceiveObject`

    """

    emitted: datetime = field(default=datetime.min)
    queued: datetime = field(default=datetime.min)
    command: ProtocolCommand = field(default=ProtocolCommand.NOP)
    data: bytearray = field(default_factory=empty_bytearray)
    id: int = field(default=0)
    time_out: float = field(default=2)
    resend: int = field(default=0)

    def decode(self) -> bytearray:
        """
        decode SendObject to bytearray
        """

        buf = bytearray()
        buf += self.command.value
        buf += DELIMITER
        buf += bytearray([len(self.data)])
        buf += self.data
        buf += bytearray([self.id])
        buf = BOT + buf
        buf += EOT
        return buf

    def __repr__(self) -> str:
        return (
            "SendObject("
            f"id: {self.id},"
            f"Command: {self.command}, "
            f"Time out: {self.time_out}, "
            f"Resend: {self.resend}, "
            f"emitted: {self.emitted.strftime('%H:%M:%S')}, "
            f"queued: {self.queued.strftime('%H:%M:%S')}"
            ")"
        )


def make_event() -> Event:
    evt = Event()
    return evt


@dataclass
class SerialObject:
    """
    serial object
    """

    id: int
    send_object: Optional[SendObject]
    receive_object: Optional[ReceiveObject]
    emitted: Optional[datetime]
    received: Optional[datetime]
    error: Event = field(default_factory=make_event)
    timeout_thread: Thread = field(default=Thread())
    completed: Event = field(default_factory=make_event)
    time_out: float = field(default=0)
    time_out_event: Event = field(default_factory=make_event)
    to_be_cleared: Event = field(default_factory=make_event)
    wait_for_clear_thread: Thread = field(default=Thread())

    def __post_init__(self):
        """
        automatic remove objects after 30 seconds
        """
        self.to_be_cleared.clear()
        self.wait_for_clear_thread = Thread(target=self._clear)
        self.wait_for_clear_thread.start()

    def _clear(self):
        if not self.to_be_cleared.wait(30):
            self.to_be_cleared.set()

    def start_timer(self):
        # Start a separate thread to set the timeout event
        LOG.debug("Starting timer")
        if self.send_object:
            LOG.debug(
                f"Starting time out for "
                f"Command: `{self.send_object.command.value}`"
                f"Data: {self.send_object.data} "
            )
            self.timeout_thread = Thread(target=self.set_timeout_event)
            self.timeout_thread.name = (
                f"SerialObject: {self.id} - {self.send_object.command!r}"
            )
            self.timeout_thread.daemon = True
            self.timeout_thread.start()

    def set_timeout_event(self):
        # Wait for the specified timeout and then set the timeout event
        LOG.info(
            (
                f"Waiting for timeout {self.time_out} "
                f"for {self.timeout_thread.name}"
            )
        )
        if not self.time_out_event.wait(self.time_out):
            if not self.completed.is_set():
                self.time_out_event.set()
                LOG.info(
                    f"Timeout set to {self.time_out_event.is_set()} "
                    f"after {self.time_out}"
                )
            else:
                self.is_completed()
                self.time_out_event.clear()

    def is_completed(self) -> bool:
        if self.send_object and self.receive_object:
            self.completed.set()
            return True
        self.completed.clear()
        return False


class SerialCom:
    def __init__(self) -> None:
        self._serial = Serial()

    def setup(self):
        LOG.info("Serial Setup started")
        try:
            if self._serial:
                self._serial.close()
            self._serial = Serial(SERIAL_PORT, baudrate=115200, timeout=0.01)
        except SerialException:
            # if reset_serial():
            #     self.setup()
            # else:
            raise CommunicationException("Failed to setup Serial")
        if self._serial.is_open:
            self._serial.close()
            self._serial.open()
            self._serial.reset_input_buffer()
            self._serial.reset_output_buffer()
            self._serial.flush()
            goOne = time.monotonic() + 15
            exit_flag = False
            while time.monotonic() < goOne and not exit_flag:
                _ = self._serial.read(200)
                if not _:
                    LOG.debug("Serial Setup no more data to read continuing")
                    exit_flag = True
            if time.monotonic() > goOne:
                LOG.debug("Continuing due to timeout.no more data to read")
        LOG.info("Serial Setup done")

    def write(self, data: bytearray) -> None:
        try:
            self._serial.write(data)
        except SerialException:
            self.setup()
            self._serial.write(data)

    def read(self, count: int = 1) -> bytearray:
        try:
            return bytearray(self._serial.read(count))
        except SerialException:
            self.setup()
            return bytearray(self._serial.read(count))


def NullFunction():
    """
    NOP function
    """
    return


class SerialManagerCommand(Enum):
    # get serial object with ID
    NOP = 0
    GET_OBJECT = 1
    GET_SAUNAREGISTER = 2
    SET_SESSION_TIME = 3
    SET_HEATER_STATE = 4


class SaunaControllerCommand(Enum):
    NOP = 0
    START_SESSION = 1
    STOP_SESSION = 2
    GET_OBJECT = 3
    GET_SAUNAREGISTER = 3


@dataclass
class SerialCommand:
    command: SerialManagerCommand
    param: Any
