"""
alternative GPIO module to replace the RPi.GPIO on a Raspberry PI
"""
import logging
from dataclasses import dataclass, field
from typing import Any, List

LOG = logging.getLogger(__name__)


@dataclass
class Channel:
    state: bool = field(default=False)
    direction: bool = field(default=False)
    enabled: bool = field(default=False)


class AltGPIO:
    HIGH = True
    LOW = False
    OUT = True
    IN = False
    BOARD = "No Board"

    def __init__(self) -> None:
        self.warnings: bool = True
        self.channel_list: List[Channel] = []
        self.create_channels()

    def create_channels(self):
        """
        create 42 fake channels
        """
        for _ in range(42):
            self.channel_list.append(Channel())

    def setmode(self, mode: str) -> None:
        print(f"Mode {mode} set")

    def setwarnings(self, data: bool) -> None:
        self.warnings = data

    def setup(self, channel: int, direction: bool):
        self.channel_list[channel - 1].direction = direction

    def cleanup(self):
        self.create_channels()

    def output(self, channel: int, state: Any):
        self.channel_list[channel - 1].state = state
        LOG.info(f"Channel {channel} set to {state}")


GPIO = AltGPIO()
