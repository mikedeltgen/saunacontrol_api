import logging

from .environment import LOGGING_LEVEL


def setup_logging():
    """
    setup project loggers
    """
    mqtt_logger = logging.getLogger("paho.mqtt.client")
    mqtt_logger.setLevel(level=LOGGING_LEVEL)

    proto_logger = logging.getLogger("sauna.proto")
    proto_logger.setLevel(level=LOGGING_LEVEL)
    controller_logger = logging.getLogger("sauna.controller")
    controller_logger.setLevel(level=LOGGING_LEVEL)
    scheduler_logger = logging.getLogger("sauna.controller.scheduler")
    scheduler_logger.setLevel(level=LOGGING_LEVEL)
    sauna_monitor_logger = logging.getLogger("sauna.monitor")
    sauna_monitor_logger.setLevel(level=LOGGING_LEVEL)
    sauna_api_logger = logging.getLogger("sauna.api")
    sauna_api_logger.setLevel(level=LOGGING_LEVEL)
    sauna_register_logger = logging.getLogger("sauna.saunaregister")
    sauna_register_logger.setLevel(level=LOGGING_LEVEL)
